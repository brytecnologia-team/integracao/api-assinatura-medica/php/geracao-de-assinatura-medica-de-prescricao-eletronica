<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AssinadorPdfController extends Controller
{
    // Token de autenticação gerado no BRy Cloud
    const access_token = "";

    public function assinarKMS(Request $request)
    {
        // Verifica se o documento enviado na requisição é válido.
        if ($request->hasFile('documento') && $request->file('documento')->isValid()) {
            $upload = $request->documento->storeAs('documentos', 'documentoParaAssinatura.pdf');
        } else {
            return response()->json(['message' => 'Arquivo enviado inválido'], 400);
        }

        // Recupera as credenciais que serão utilizadas
        $kms_credencial_tipo = $request->tipo_credencial;
        $kms_credencial = $request->valor_credencial;

        // Verifica se a imagem enviada na requisição é valida.
        if ($request->incluirIMG === 'true') {
            if ($request->hasFile('imagem') && $request->file('imagem')->isValid()) {
                $upload = $request->imagem->storeAs('imagem', 'imagemAssinatura.png');
            } else {
                return response()->json(['message' => 'Imagem enviada inválida'], 400);
            }
        }

        // Verifica se informações sobre o CPF do signatário foram enviadas
        $signatario_info = "";
        if ($request->signatario) {
            $signatario_info = '"signatario" : "' . $request->signatario . '",';
        }

        $data = array(
            'dados_assinatura' =>
            '{
            ' . $signatario_info . '
            "algoritmoHash" : "' . $request->algoritmoHash . '",
            "perfil" : "' . $request->perfil . '",
            "tipoRetorno" : "link"
            }',
            'documento' => new \CURLFILE(storage_path() . '/app/documentos/documentoParaAssinatura.pdf'),
        );

        // Verifica se o profissional informado é médico e, caso seja, insere também as OIDs do farmacêutico
        if ($request->numeroOID === "2.16.76.1.4.2.2.1") {
            $data['metadados'] = '{
                "' . $request->tipoDocumento . '" : "",
                "' . $request->numeroOID . '" : "' . $request->numero . '",
                "2.16.76.1.4.2.3.1" : "",
                "' . $request->UFOID . '" : "' . $request->UF . '",
                "2.16.76.1.4.2.3.2" : "",
                "' . $request->especialidadeOID . '" : "' . $request->especialidade . '",
                "2.16.76.1.4.2.3.3" : ""
                }';
        } else {
                $data['metadados'] = '{
                    "' . $request->tipoDocumento . '" : "",
                    "' . $request->numeroOID . '" : "' . $request->numero . '",
                    "' . $request->UFOID . '" : "' . $request->UF . '",
                    "' . $request->especialidadeOID . '" : "' . $request->especialidade . '"
                    }';
        }

        if ($request->assinaturaVisivel === 'true') {
             $data['configuracao_imagem'] = '{
                "altura" : "' . $request->altura . '",
                "largura" : "' . $request->largura . '",
                "coordenadaX" : "' . $request->coordenadaX . '",
                "coordenadaY" : "' . $request->coordenadaY . '",
                "posicao" : "' . $request->posicao . '",
                "pagina" : "' . $request->pagina . '"
                }';

                $data['configuracao_texto'] = '{
                "texto" : "' . $request->texto . '",
                "incluirCN" : "' . $request->incluirCN . '",
                "incluirCPF" : "' . $request->incluirCPF . '",
                "incluirEmail" : "' . $request->incluirEmail . '"
                }';
                    // Configurações da imagem da assinatura
                    if ($request->incluirIMG === 'true') {
                        $data['imagem'] = new \CURLFILE(storage_path() . '/app/imagem/imagemAssinatura.png');
                        };
        }

        // Cria o CURL que será enviada à API de Assinatura.
        $curl = curl_init();

        curl_setopt_array($curl, array(
            //CURLOPT_URL => "https://hub2.hom.bry.com.br/fw/v1/pdf/kms/lote/assinaturas",
            CURLOPT_URL => "https://hub2.bry.com.br/fw/v1/pdf/kms/lote/assinaturas",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => $data,
            CURLOPT_HTTPHEADER => array(
                'Authorization: ' . self::access_token,
                'kms_credencial: ' . $kms_credencial,
                'kms_credencial_tipo: ' . $kms_credencial_tipo,
            ),
        ));

        $response = curl_exec($curl);
        $info = curl_getinfo($curl);
        curl_close($curl);

        $status_code = $info['http_code'];
        $objResponse = json_decode($response);
        if ($status_code != 200) {

            return response(array('message' => $objResponse->message), $status_code);
        }

        // Retorno o link do documento assinado para ser baixado no front-end
        return $objResponse->documentos[0]->links[0]->href;
    }
}
